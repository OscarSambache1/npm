module.exports = (usuarioNuevo, callback) => {
    const datosPrueba = require('./datosPrueba')
    let usuarioACrear = new Object();
    usuarioACrear.nombre = usuarioNuevo
    if (usuarioNuevo != '') {
        datosPrueba.arregloUsuarios.push(usuarioACrear)
        callback({
            arregloUsuarios: datosPrueba.arregloUsuarios,
            mensaje: `èl usuario ${usuarioACrear.nombre} fue creado`
        })
    }
    else{
        callback({
            arregloUsuarios: datosPrueba.arregloUsuarios,
            mensaje: `èl usuario no fue creado`
        })
    }
}