module.exports =(usuarioBuscarCrear, callback )=>{
    const metodos= require('./paquetes.js')
    metodos.buscarUsuario(usuarioBuscarCrear, (resultado) =>{
        if(resultado.arregloUsuariosEncontrados.length !== 0){
            callback({
                mensaje: resultado.mensaje,
                posicionUsuario: resultado.posicionUsuario,
                usuarioEncontrado: resultado.arregloUsuariosEncontrados[0],
                arregloUsuariosEncontrados: resultado.arregloUsuariosEncontrados
            })
        }
        else {
            metodos.crearUsuario(usuarioBuscarCrear, (resultado)=> {
                callback({
                    mensaje: resultado.mensaje,
                    arregloUsuarios: resultado.arregloUsuarios
                })
            })
            
        }
    })
}