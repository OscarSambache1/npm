const buscarCrearUsuario = require('./buscar-crear.js')
const eliminarUsuario = require('./eliminar.js')
const crearUsuario = require('./crear.js')
const buscarUsuario = require('./buscar.js')
const buscarPorExpresionRegular = require('./buscarPorExpresionRegular.js')
const datosPrueba = require('./datosPrueba.js')


module.exports = {
    buscarCrearUsuario,
    eliminarUsuario,
    crearUsuario,
    buscarUsuario,
    buscarPorExpresionRegular,
    datosPrueba
}

