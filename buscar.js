module.exports = ( usuarioABuscar, callback) => {
    const datosPrueba = require('./datosPrueba')
    let posicionUsuario = -1;

    console.log(usuarioABuscar)
    if(usuarioABuscar!=''){
        let arregloUsuariosEncontrados = datosPrueba.arregloUsuarios.map((valor, indice) => {
            let existeUsuario = valor.nombre.toLowerCase() === usuarioABuscar.toLowerCase()
            if (existeUsuario) {
                posicionUsuario = indice
                return valor
            }
        }).filter(valor => {
            return valor !== undefined
        })
        if (arregloUsuariosEncontrados.length !== 0) {
            callback({
            
        posicionUsuario: posicionUsuario,
                mensaje: `El usuario ${arregloUsuariosEncontrados[0].nombre} fue encontrado en el indice ${posicionUsuario}`,
                usuarioEncontrado: arregloUsuariosEncontrados[0],
                arregloUsuariosEncontrados
            })
        }
        else {
            callback({
                posicionUsuario: posicionUsuario,
                mensaje: `èl usuario no fue encontrado`,
                usuarioEncontrado: arregloUsuariosEncontrados[0],
                arregloUsuariosEncontrados
            })
        }
    }
    else{
        callback({
            posicionUsuario: posicionUsuario,
            mensaje: `èl usuario no fue encontrado`,
            usuarioEncontrado: arregloUsuariosEncontrados[0],
            arregloUsuariosEncontrados
        })
    }
    
}
