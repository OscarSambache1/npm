module.exports = ( usuarioAEliminar, callback)=>{
    const metodos= require('./paquetes.js')
    const datosPrueba = require('./datosPrueba')
    metodos.buscarUsuario(usuarioAEliminar, (resultado)=>{
        if(resultado.posicionUsuario != -1){
            datosPrueba.arregloUsuarios.splice(resultado.posicionUsuario, 1)
            callback({
                mensaje: `èl usuario ${usuarioAEliminar} fue eliminado`,
                arregloUsuarios: datosPrueba.arregloUsuarios
            })
        }
        else {

            callback({
                mensaje: `${resultado.mensaje} por lo que no pudo ser eliminado`
            })
        }
    } )
    
}